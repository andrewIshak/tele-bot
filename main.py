import os
import telebot
import time

API_KEY = '5710731613:AAGalit2wBCXVfhgY0O1alEXUgWFwxKYsLg'
bot = telebot.TeleBot(API_KEY)

@bot.message_handler(commands=['test'])
def test(message):
  bot.reply_to(message, "works")
  
@bot.message_handler(commands=['start'])
def start(message):
  bot.reply_to(message, "Hello Monica!! \U0001F44B")
  time.sleep(2)
  bot.send_message(message.chat.id, "How are you ?!")
  time.sleep(2)
  bot.send_message(message.chat.id,
                   "I hope you are fine and well \U0001F44D \U0001F91B")
  time.sleep(2)
  bot.send_message(message.chat.id, "*First of all*", parse_mode='Markdown')
  time.sleep(2)
  bot.send_animation(
    chat_id=message.chat.id,
    animation=
    "https://i.pinimg.com/originals/ff/6e/bd/ff6ebd0dfb50a44c04c842f365df4446.gif"
  )
  bot.send_message(
    message.chat.id,
    "Merry Christmas!!! \U0001F384 \U0001F386 May your happiness be large and your bills be small. \U0001F602 \U0001F602"
  )
  time.sleep(2)
  bot.send_message(
    message.chat.id,
    "You are a good,kind and pure girl hope you receive one blessing after another this coming year \U0001FAE7"
  )
  time.sleep(2)
  bot.send_message(message.chat.id,
                   """*Who am I?* 😅😶
I'm your Christmas bot🤖 🎄 
I was made by your secret santa🎅 🎅 to make you have some nice time⏳ (hope so🙏) """,
                   parse_mode='Markdown')
  time.sleep(2)
  bot.send_message(message.chat.id,
                   """
*What are we going to do?* 
I will give you a little puzzle, and you must solve it before Thursday 7 pm ⏰  to get a Christmas gift 🎁 (life is not easy)
So if you want to continue and get your puzzle type "/puzzle"😀 😀 and if you don't want to solve any puzzles then type "/gift"🙄 😅 """,
                   parse_mode='Markdown')
  time.sleep(2)
  bot.send_message(
    message.chat.id,
    "‼️ *Note:- I am taking commands only and commands starting with '/'* ",
    parse_mode='Markdown')


@bot.message_handler(commands=['gift'])
def gift(message):
  bot.reply_to(
    message,
    "As I told you before (life is not easy)😅 😂 you must solve the puzzle so type \"/puzzle\" to start."
  )


@bot.message_handler(commands=['puzzle'])
def puzzle(message):
  bot.send_message(message.chat.id,
                   """
    *Before we start let me clarify some points* 👓 🫤  :- 
    1- the puzzle is easy i will give you a picture 🖼️ with a command *(a word starting with '/')* written on it , you should extract this command and type it to me.
    2- you have 3 hints which you can use, but ( every thing has a cost😇 😂  )
    to use the first hint you will wait 1 hour , 2 hours for the second and  3 hours for the third one ( this time is to giveing you a chance to think and solve it by your own💪 💪  ) every hint has it is command the hint 1 command is  \"/hint\".
    3- you can get any one help 
    4- if you have any questions , or a connection error happen send message to Mina Medhat
    """,
                   parse_mode='Markdown')
  time.sleep(2)
  bot.send_message(message.chat.id,
                   "*Are You Ready ?!?!?!?!?!* 👊 👊 ",
                   parse_mode='Markdown')
  time.sleep(10)
  bot.send_message(message.chat.id, "5️⃣")
  time.sleep(2)
  bot.send_message(message.chat.id, "4️⃣")
  time.sleep(2)
  bot.send_message(message.chat.id, "3️⃣")
  time.sleep(2)
  bot.send_message(message.chat.id, "2️⃣")
  time.sleep(2)
  bot.send_message(message.chat.id, "1️⃣")
  time.sleep(2)
  bot.send_message(message.chat.id, "Here is your puzzle picture")
  bot.send_photo(chat_id=message.chat.id,
                 photo=open('puzzle.jpg', 'rb'),
                 caption="",
                 disable_notification=False)
  bot.send_message(
    message.chat.id,
    """‼️ *Note:- if you want to use the first hint just type \"/hint\" and i will send your first hint after 1 hour.
  send \"/hint\" one time .... every time you send \"/hint\" to me i will start count the time from the begining.*""",
    parse_mode='Markdown')
  bot.send_message(message.chat.id, "*Good luck!*", parse_mode='Markdown')


@bot.message_handler(commands=['hint'])
def hint(message):
  bot.reply_to(
    message,
    "I will send your first hint in 59.59 minutes..... and i will remind you every 10 minutes with the remaining time ."
  )
  time.sleep(600)
  bot.send_message(message.chat.id, "The remaining time is 50 minutes")
  time.sleep(600)
  bot.send_message(message.chat.id, "The remaining time is 40 minutes")
  time.sleep(600)
  bot.send_message(message.chat.id, "The remaining time is 30 minutes")
  time.sleep(600)
  bot.send_message(message.chat.id, "The remaining time is 20 minutes")
  time.sleep(600)
  bot.send_message(message.chat.id, "The remaining time is 10 minutes")
  time.sleep(541)
  bot.send_message(message.chat.id, "The remaining time is 59 seconds")
  time.sleep(59)
  bot.send_message(message.chat.id,
                   "*Hint 1 :- it is a Stereogram picture.*",
                   parse_mode='Markdown')
  time.sleep(5)
  bot.send_message(
    message.chat.id,
    """‼️ *Note:- if you want to use the second hint just type \"/hint02\" and i will send your second hint after 2 hours.
  send \"/hint02\" one time .... every time you send \"/hint02\" to me i will start count the time from the begining.*""",
    parse_mode='Markdown')


@bot.message_handler(commands=['hint02'])
def hint02(message):
  bot.reply_to(
    message,
    "I will send your second hint in 119.59 minutes..... and i will remind you every 15 minutes with the remaining time ."
  )
  time.sleep(900)
  bot.send_message(message.chat.id, "The remaining time is 105 minutes")
  time.sleep(900)
  bot.send_message(message.chat.id, "The remaining time is 90 minutes")
  time.sleep(900)
  bot.send_message(message.chat.id, "The remaining time is 75 minutes")
  time.sleep(900)
  bot.send_message(message.chat.id, "The remaining time is 60 minutes")
  time.sleep(900)
  bot.send_message(message.chat.id, "The remaining time is 45 minutes")
  time.sleep(900)
  bot.send_message(message.chat.id, "The remaining time is 30 minutes")
  time.sleep(900)
  bot.send_message(message.chat.id, "The remaining time is 15 minutes")
  time.sleep(841)
  bot.send_message(message.chat.id, "The remaining time is 59 seconds")
  time.sleep(59)
  bot.send_message(
    message.chat.id,
    "*Hint 2 :- This link will help you https://www.wikihow.com/View-Stereograms .*",
    parse_mode='Markdown')
  time.sleep(5)
  bot.send_message(
    message.chat.id,
    """‼️ *Note:- if you want to use the third hint just type \"/hint003\" and i will send your third hint after 3 hours.
  send \"/hint003\" one time .... every time you send \"/hint003\" to me i will start count the time from the begining.*""",
    parse_mode='Markdown')


@bot.message_handler(commands=['hint003'])
def hint003(message):
  bot.reply_to(
    message,
    "I will send your second hint in 179.59 minutes..... and i will remind you every 30 minutes with the remaining time ."
  )
 time.sleep(1800)
  bot.send_message(message.chat.id, "The remaining time is 150 minutes")
  time.sleep(1800)
  bot.send_message(message.chat.id, "The remaining time is 120 minutes")
  time.sleep(1800)
  bot.send_message(message.chat.id, "The remaining time is 90 minutes")
  time.sleep(1800)
  bot.send_message(message.chat.id, "The remaining time is 60 minutes")
  time.sleep(1800)
  bot.send_message(message.chat.id, "The remaining time is 30 minutes")
  time.sleep(1741)
  bot.send_message(message.chat.id, "The remaining time is 59 seconds")
  time.sleep(59)
  bot.send_message(
    message.chat.id,
    "*Hint 3 :- Use this link https://magiceye.ecksdee.co.uk/ .*",
    parse_mode='Markdown')


@bot.message_handler(commands=['2023'])
def finish(message):
  bot.reply_to(message, "*Congratulations* 🥳 💣 🍬 💥 💥 You made it.")
  bot.send_message(
    message.chat.id,
    "For your gift send this to Mina Medhet (Happy new year) .... to get your first gift🧧 🎁 ."
  )
  time.sleep(5)
  bot.send_message(
    message.chat.id,
    """And there are more ,For your second gift (My gift as a bot to you🤖  🎁 ) ...... 
    it will be ..... 
    some thing you want ......🤔 💭 """)
  bot.send_photo(
    chat_id=message.chat.id,
    photo=
    "http://www.dream-catchers.org/wp-content/uploads/2015/02/sunset_dreamcatcher-200x300.png"
  )
  time.sleep(10)
  bot.send_message(message.chat.id, "5️⃣")
  time.sleep(2)
  bot.send_message(message.chat.id, "4️⃣")
  time.sleep(2)
  bot.send_message(message.chat.id, "3️⃣")
  time.sleep(2)
  bot.send_message(message.chat.id, "2️⃣")
  time.sleep(2)
  bot.send_message(message.chat.id, "1️⃣")
  time.sleep(2)
  bot.send_message(
    message.chat.id,
    """Here is some links which can help you to make your dream catcher
  https://www.youtube.com/watch?v=Vmtkc7FOWLw
  https://www.youtube.com/watch?v=EUulfZU3AQY
  https://www.youtube.com/watch?v=hDE1_Af8xPY
  😂😂😂😂😂😂😂😂😂😂😂😂😂😂😂""")
  time.sleep(5)
  bot.send_message(
    message.chat.id, """
  Once again I hope you enjoyed your time with me 🥳 🥳  and I hope 2023 will be good year for you.
Tahnks for your time , I hope you enjoyed
Bye bye 👋 👋 """)


bot.polling()
